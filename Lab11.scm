(define Yfunc
(lambda(m)
(f (m m)) (f (m m))))

(define loop 
(lambda(m)
((isZero m)0 ADD f(m-1) m)))

(define func
(lambda (m n)
(OR (LESS m n) (isZero m) ( Yfunc loop n) (ADD m n))))